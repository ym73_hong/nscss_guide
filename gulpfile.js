'use strict';

const { series } = require('gulp');
const sass = require('./gulp/sass');


exports.build = series(sass.build);
exports.watch = series(sass.build, (cb) => {
	sass.watch();
	
	cb();
});