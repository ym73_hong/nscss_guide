'use strict';

module.exports = (() => {
	const {src, dest, watch} = require('gulp');
	const gulp_sass = require('gulp-sass');
	const gulp_postcss = require('gulp-postcss');
	const autoprefixer = require('autoprefixer');
	const gulp_sourcemaps = require('gulp-sourcemaps');
	
	const options = require('./options');
	const msg = require('./message');
	const beep = require('./beep');

	const basePath = `./sample`; 
	
	gulp_sass.compiler = require('sass');
	options.sass.fiber = require('fibers');
	
	const build = () => {
		const source = [`${basePath}/sass/style.scss`];
		
		return src(source, options.src)
			.pipe(gulp_sourcemaps.init())
			.pipe(gulp_sass(options.sass))
			.pipe(gulp_postcss([autoprefixer(options.autoprefixer.sass)]))
			.pipe(gulp_sourcemaps.write('.', {includeContent: true, sourceRoot: '../sass'}))
			.pipe(dest('css', {cwd: basePath}))
			.on('end', () => {
				msg.success('SASS build');
			});
	};
	
	return {
		build: build,
		watch() {
			const source = [`${basePath}/sass/**/*.scss`];
			const watcher = watch(source, options.watch, build);
			
			watcher.on('all', (path, state) => {
				msg.log(path, state);
			});
			watcher.on('error', () => {
				beep();
			});
		},
	};
})();