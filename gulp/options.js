'use strict';

module.exports = (() => {
	return {
		sass: {
			outputStyle: 'expanded',
			// precision: 8	//bootstrap 3.x 호환을 위해 설정함, but Dart SASS 사용시 10자리로 고정되므로 필요 없음
		},
		
		autoprefixer: {
			sass: {
				cascade: false,
				grid: 'no-autoplace',
				overrideBrowserslist: [
					'last 1 chrome version',
					'last 1 android version',
					'last 1 and_chr version',
					'last 1 edge version',
					'last 1 Firefox version',
					'last 1 ios_saf version',
					'last 1 Safari version',
					'ie 10-11',
				]
			},
		},
		
		watch: {
			ignoreInitial: true,
			events: 'all',
			alwaysStat: false,
			useFsEvents: true,
			awaitWriteFinish: true,
		},
		
		src :{
			nosort: true,
			allowEmpty: true,
			strict: true,
		},
		
	};
})();