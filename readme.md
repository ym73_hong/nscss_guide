# NSCSS ?

**Non-Semantic CSS Naming Convention** (NSCSS) 의 약자로, UI 의 구조 및 의미에 종속되지 않는 CSS naming convention 을 말합니다.



## 목차

1. [NSCSS 탄생](01.md)
2. [주요 특징](02.md)
3. [문법](03.md)
4. [튜토리얼 : Local](04_01.md)
5. [튜토리얼 : Global](04_02.md)
6. Vue 컴포넌트 과 결합
7. 기본 유형
