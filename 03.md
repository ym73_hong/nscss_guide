# 문법



## 구조

### Global

![image-20200803161532921](image/image-20200803161532921.png)

- 공용 스타일을 지칭 합니다.
- **Global Prefix** 와 **Type** , **Distinction** 3 부분으로 구성됩니다.

### Local

![image-20200803163006964](image/image-20200803163006964.png)

- 단일 **Container** 와 종속된 복수의 **Leaf** 로 구성됩니다.
- BEM 의 Block 과 동일한 의미를 가지고 있으나 **어떠한 CSS 속성도 지정되어서는 안됩니다.**



## 공통 사항

- 대문자를 사용하지 않습니다
- 각 항목은 - 로 구분 합니다. 단 Leaf Prefix 는 예외 입니다.
- _ 사용은 Container Name 과 Type 내에서만 허용 합니다.



## 각 항목에 대한 설명

### Global prefix

- 공용 스타일을 구별 하기 위해 사용합니다.
- 단일 문자 c 를 권장 하며, 이미 c 가 사용중이면 다른 문자를 사용해도 무방합니다.

### Type

- UI 를 표현하기 위한 CSS 속성들의 집합

- UI 와 연관성을 배제하기 위해 CSS 의 기능 및 유형을 기준으로 일반적인 단어를 사용합니다.

- 복합 명사로 구성되거나 단어가 길 경우에 한해  _ 를 사용할 수 있습니다.
    
- 예) 로그인을 위한 ID, Password 입력 화면에 CSS Grid 를 사용중일 경우
    | Best                                               | Wrost                      |
    | -------------------------------------------------- | -------------------------- |
    | CSS Grid 관련 CSS 속성들만 묶어서 **grid** 로 명명 | login_grid, login_form--id |

### Distinction

- Type 의 세부 유형을 구분하기 위해 사용 합니다.
- 의미있는 단어를 사용할 수 있으나, 추상화 및 리펙토링을 위해 숫자로 할 것을 권장 합니다.
- 단, Type 만으로 유니크 할 수 있으면 사용하지 않아도 무방합니다.
- 예를 들어 카드별 할부 정보가 있는 테이블 유형의 UI 는 _table-1 또는 c-table-1 , 사용자 정보가 있는 테이블 유형의 UI 는 _table-2 또는 c-table-2 로 구분 합니다.
- 
- 

### Container Type

- Container 에 종속되어 있는 Leaf 를 나타내기 위해 반드시 _ 로 시작합니다.
- Global Type 과 마찬가지로 추상화를 위해 CSS 와 html 의 기능 및 유형을 기준으로 일반적인 단어를 사용합니다.
- 예를 들어 CSS Grid 를 사용하는 다양한 목적의 UI 가 있을 경우, 목적을 배제하고 _grid 로 명명 합니다.

### Container

- 종속된 복수의 Leaf 를 가지며, 다른 Container 의 일부가 될 수 없습니다.
- Container 는 복수의 Leaf 를 특정하는 역할만 합니다. 즉, Container 에는 어떤 스타일도 지정되어선 안됩니다.
- Container 의 이름은 NSCSS 에서 유일하게 UI 가 나타내는 의미를 가질 수 있습니다. 즉 product_list 이런 식의 명명이 가능합니다.

